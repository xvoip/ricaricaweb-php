<?php

class tc_db {
	
	var $dblink;
	
	public function __construct() {
	}

	public function connect($conf=array()) {
		$this->dblink = mysql_connect($conf["dbhost"], $conf["dbuser"], $conf["dbpass"]);
		if ($this->dblink)
			mysql_select_db($conf["dbname"], $this->dblink);
		else
			print "connection not established:".mysql_errno($this->dblink);
	}
	
	public function query($q) {
		$r = mysql_query($q, $this->dblink);
		return $r;
	}
	
	public function select($q) {
		$r = $this->query($q);
		$rar = array();
		while($rr = mysql_fetch_object($r))
			$rar[] = $rr;
		return $rar;
	}
	
}
