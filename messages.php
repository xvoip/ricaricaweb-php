<?php

class messages {

	public $items;

	public function load($p="") {
		global $wp_version;
		$this->items = parse_ini_file(dirname(__FILE__)."/messages.ini", true);
		// Wordpress
		if ($wp_version!="") {
			$extitems = get_option( 'tcrw_settings' );
			$this->items = array_merge($this->items,array("translations"=>$extitems["translations"]));
		}
		// Joomla
		// Prestashop
		// Magento

		$this->items["translations"]["en"] = $this->items["translations-en"];
		$this->items["translations"]["it"] = $this->items["translations-it"];
		$this->items["translations"]["es"] = $this->items["translations-es"];
		$this->items["translations"]["fr"] = $this->items["translations-fr"];
		$this->items["translations"]["de"] = $this->items["translations-de"];

	}

}
