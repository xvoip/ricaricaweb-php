<?php

/*
*
*	Telecash - Ricarica Web
*	ver. 1.0
*
*/

include "messages.php";
include "config.php";
include "database.php";

class ricaricaweb {

	private $_dblink;
	private $_conf;
	private $messages;
	private $instanceid;

	public $currLang = "";

	public function __construct() {
		$this->loadConf();
	}

	public function __destruct() {}

	public function setup() {}

	// create and collect the basic info for a request
	public function getRequestBaseData($code="", $ek="") {

		$rk = ($ek!="")?$ek:$this->requestKey($code);
		$rdata = array();
		$page = explode("?", $_SERVER['REQUEST_URI'], 2);
		$rdata["tc_url_ok_dinamic"] = "http://".$_SERVER['HTTP_HOST'].$page[0]."?rres=OK";
		$rdata["tc_url_ko_dinamic"] = "http://".$_SERVER['HTTP_HOST'].$page[0]."?rres=KO";
		$rdata["tc_payment_notification"] = "http://".$_SERVER['HTTP_HOST'].$page[0]."?mng=1";
		$rdata["tc_custom1"] = $rk;
		return $rdata;

	}

	public function getForm($conf=array()) {
		
		$this->instanceid = $conf;
		$conf = array_merge($this->_conf["general"], $this->_conf["instance-$conf"]);

//		$conf = array_merge($conf, $this->getRequestBaseData($conf["tc_alias"], $_REQUEST["mres"]));
		$conf = array_merge($conf, $this->getRequestBaseData($conf["tc_merchant"].$conf["tc_alias"], @$_REQUEST["mres"]));
		$rows = array();

		if ($conf["tc_merchant"]=="")
			return $rows[] = $this->messages["error_messages"]["ERR_MSG_NO_INSTANCE_FOUND"];

		$cssclass = "";
		$cssclass .= ($conf["tc_has_paypal"]=="1")?"":"nopaypal";
		$action = ($conf["tc_affiliate_merchant"]!="")?"http://panel.moneymaster.it/rpc/send.php":$conf["tc_url"];
		$rows[] = "<form action='".$action."' method='POST' id='form_tc_service' class='$cssclass' onsubmit=\"return checkStartPay();\">";
		$inopf = array("tc_usetcjs","tc_url","tc_taglio","tc_require_customer_email","tc_has_paypal","tc_template","template","tc_disable_paypal","tc_affiliate_merchant","tc_doublenum","tc_coupon_enable","tc_coupon_enable_default","tc_coupon_default","tc_coupon_default_hidden");
		$conf["tc_custom2"] = $conf["tc_affiliate_merchant"];

		if ($conf["tc_usetcjs"]=="1") {
			$inopf[] = "tc_country";
			$usetcjs=true;
		}
		foreach ($conf as $bfk => $bfv) {
			if (!in_array($bfk, $inopf)) {
				if ($bfv!="") {
					if ($bfk=="tc_set_phone_credit"&&$bfv=="1")
						$rows[] = "<input type=hidden id='$bfk' name='".substr($bfk,3)."' value='0' />";
					else
						$rows[] = "<input type=hidden id='$bfk' name='".substr($bfk,3)."' value='$bfv' />";
				}
			}
		}

		if ($conf["tc_coupon_enable"]=="1") {
			if ($conf["tc_coupon_enable_default"]=="1")
				$rows[] = "<input type=hidden id='tc_coupon' name='coupon' value='".$conf["tc_coupon_default"]."' />";
		}

		if (!in_array("tc_set_phone_credit", array_keys($conf)))
			$rows[] = "<input type=hidden id='tc_set_phone_credit' name='set_phone_credit' value='1' />";

		if ($usetcjs) {
			$rows[] = "<div class='tc_field'><select id='tc_taglio' name='taglio'></select></div>";
			$rows[] = "<div class='tc_field'><select id='tc_country' name='country'></select></div>";
		} else {
			$stropts = explode(";", $conf["tc_taglio"]);
			if (count($stropts)==1) {
				$rows[] = "<input type='hidden' name='taglio' id='tc_taglio' value='".$conf["tc_taglio"]."' />";
				$wideani = 1;
			} else {
				$rows[] = "<div class='tc_field'><select id='tc_taglio' name='taglio'>";
				foreach ($stropts as $opt) {
					$pcs = explode("|", $opt);
					$rows[] = "<option value='".$pcs[0]."'>".$pcs[1]."</option>";
				}
				$rows[] = "</select></div>";
			}
		}

		$rows[] = "<div class='tc_field ".(($wideani)?"wide":"")."'><input type='text' name='ani' id='tc_ani' size=35 maxlength=35 tabindex=1 placeholder='".$this->messages["translations-$this->currLang"]["tc_string_yourphonenumber"]."' /></div>";

		if ($conf["tc_coupon_enable"]=="1") {
			if ($conf["tc_coupon_enable_default"]=="1"&&$conf["tc_coupon_default_hidden"]!="1")
				$rows[] = "<div class='tc_field'><span class='coupon'>".$conf["tc_coupon_default"]."</span></div>";
			elseif ($conf["tc_coupon_enable_default"]!="1")
				$rows[] = "<div class='tc_field'><input type=text id='tc_coupon' name='coupon' value='' placeholder='".$this->messages["translations"][$this->currLang]["tc_string_insertcoupon"]."' /></div>";
		}

		// payment method
		switch ($conf["tc_disable_paypal"]) {
			case "0": // paypal and cc
/*
				$rows[] = "<div class='tc_field wide'>
					<label class='pwselector'><input type='radio' name='disable_paypal' id='tc_disable_paypal' placeholder='' value='0' checked='checked' class='paypal' />&nbsp;<img src='/wp-content/plugins/tc_ricaricaweb/templates/images/paypal.png' border='0' align='absmiddle' /></label>
					<label class='pwselector'><input type='radio' name='disable_paypal' id='tc_disable_paypal' placeholder='' value='1' class='creditcard' />&nbsp;<img src='/wp-content/plugins/tc_ricaricaweb/templates/images/credit-card.png' border='0' align='absmiddle' /></label>
					</div>";
*/
				$rows[] = "<div class='tc_field wide'>
					<label class='pwselector paypal'><input type='radio' name='disable_paypal' id='tc_disable_paypal' placeholder='' value='0' class='paypal' onclick='switchRadio(\"tc_disable_paypal\", this);' /></label>
					<label class='pwselector creditcard'><input type='radio' name='disable_paypal' id='tc_disable_paypal' placeholder='' value='1' class='creditcard' onclick='switchRadio(\"tc_disable_paypal\", this);' /></label>
					</div>";
				$conf["template"] = dirname(__FILE__)."/templates/tc_ricaricaweb_both.tpl";
				break;
			case "1": // only paypal
				$rows[] = "<input type='hidden' name='disable_paypal' id='tc_disable_paypal' value='0'>";
				$conf["template"] = dirname(__FILE__)."/templates/tc_ricaricaweb_paypal.tpl";
				break;
			case "2": // only cc
				$rows[] = "<input type='hidden' name='disable_paypal' id='tc_disable_paypal' value='1'>";
				$conf["template"] = dirname(__FILE__)."/templates/tc_ricaricaweb_cc.tpl";
				break;
		}

		if ($conf["tc_require_customer_email"]=="1")
			$rows[] = "<div class='tc_field'><input type='text' name='customer_email' id='tc_customer_email' size=35 maxlength=100 tabindex=2 placeholder='".$this->messages["translations-$this->currLang"]["tc_string_youremail"]."' /></div>";

		$rows[] = "<div class='tc_button wide'><input type='submit' value='".$this->messages["translations-$this->currLang"]["tc_string_pay"]."' name='B1' /></div>";
//		$rows[] = "<div class='tc_button'><input type='reset' value='".$this->messages["translations-$this->currLang"]["tc_string_cancel"]."' name='reset' /></div>";

		$rows[] = "</form>";

		$tpl = file_get_contents($conf["template"]);
		$out = str_replace("##fields##", join("", $rows), $tpl);

//		return join("", $rows);
		return $out;

	}
	
	public function showForm($conf="") {
		print $this->getForm($conf);
	}
	
	public function getJavascripts() {
		$rows = array();
		if ($this->_conf["instance-".$this->instanceid]["tc_usetcjs"]=="1") {
			$rows[] = "<script src='https://secure.tcserver.it/js/tc.js'></script>";
		}
		$rows[] = "<script src='".$this->_conf["general"]["baseurl"]."/tc_ricaricaweb/javascript/frontend.js'></script>";
		return join("\n", $rows);
	}
	
	public function showJavascripts() {
		print $this->getJavascripts();
	}

	public function getCSS() {
		$rows = array();
		$rows[] = "<link rel=stylesheet href='".$this->_conf["general"]["baseurl"]."/tc_ricaricaweb/css/frontend.css'></link>";
		return join("\n", $rows);
	}
	
	public function showCSS() {
		print $this->getCSS();
	}

	// manage the response from TC server
	public function manageResponse($conf=array()) {

		$rk = $_REQUEST["custom1"];
		$rkc = $this->validRK($rk);
		$find = preg_match("/_([A-Za-z\s]+)$/", $rkc[1]->result_descr, $statusmessage);

		if ($rkc[0]) {

			$result = $_REQUEST["result"];
			$rescode = $_REQUEST["result_code"];
			$resmsg = $this->messages["return_codes"][$_REQUEST["result_code"]];

			if ($_REQUEST["rres"]=="OK") {
				$result = "<div class='tc_message_confirm'>".$this->messages["translations-$this->currLang"]["tc_string_paymentok"]."</div>";
			}

			if ($_REQUEST["rres"]=="KO") {
				$ERRMSG = $this->messages["return_codes"][$_REQUEST["result_code"]];
				$result = "<div class='tc_message_error'>".$this->messages["translations"][$this->currLang]["tc_string_paymenterr"]."<br />Errore: ".$ERRMSG."</div>";
			}

			if ($_REQUEST["mng"]=="1") {

				$status = ($_REQUEST["result"]=="OK")?"validated":"failed";
				$q = "update tcrw_tickets set merchant='%s',service_type=%s,status='%s',transaction_id='%s',result='%s',result_code='%s',result_descr='%s',amount='%s',divisa='%s',endat='%s',credit_time='%s',service_phone='%s',ps='%s', customer_email='%s' where rk='$rk'";
				$q = sprintf($q,
	                                $_REQUEST["merchant"],
	                                $_REQUEST["service_type"],
	                                $status,
	                                $_REQUEST["transaction_id"],
	                                $_REQUEST["result"],
	                                $_REQUEST["result_code"],
	                                $_REQUEST["result_descr"],
	                                $_REQUEST["amount"],
	                                $_REQUEST["divisa"],
	                                $_REQUEST["timestamp"],
	                                $_REQUEST["credit_time"],
	                                $_REQUEST["service_phone"],
	                                $_REQUEST["ps"],
	                                $_REQUEST["customer_email"]
	                        );

				$this->_dblink->query($q);

			} else {

				$status = ($_REQUEST["result"]=="OK")?"validated":"failed";
				$q = "update tcrw_tickets set status='%s',transaction_id='%s',result='%s',result_code='%s',result_descr='%s',amount='%s',divisa='%s',endat='%s',credit_time='%s',service_phone='%s',ps='%s',customer_email='%s' where rk='$rk'";
				$q = sprintf($q,
	                                $status,
	                                $_REQUEST["transaction_id"],
	                                $_REQUEST["result"],
	                                $_REQUEST["result_code"],
	                                $_REQUEST["result_descr"],
	                                $_REQUEST["amount"],
	                                $_REQUEST["divisa"],
	                                $_REQUEST["timestamp"],
	                                $_REQUEST["credit_time"],
	                                $_REQUEST["service_phone"],
	                                $_REQUEST["ps"],
	                                $_REQUEST["customer_email"]
	                        );

				$this->_dblink->query($q);

			}

			$result .= $this->getForm($conf);
			return $result;

		} else {

			return $this->getForm($conf);

		}

	}

	// create a request key
	private function requestKey($code="") {

		$rk = md5($this->instanceid.time());
		$_SESSION["TcRw_Rk"] = $rk;
		$q = "insert into tcrw_tickets (rk,startat,status,service_phone) values ('$rk', now(), 'created', '$code')";
		$this->_dblink->query($q);
		return $rk;

	}

	// validate the current request key
	private function validRK($tcrk="") {
		$r = $this->_dblink->select("select * from tcrw_tickets where rk='$tcrk'");
//		return array(($r[0]->status!=""&&$r[0]->status!="failed"&&$r[0]->status!="cancelled"),$r);
		return array(($r[0]->id!=""),$r[0]);
	}

	private function getCurrLang() {
		switch ($this->_conf["general"]["tc_lng"]) {
			case "ENG":
				return "en";
				break;
			case "ESP":
				return "es";
				break;
			case "FRA":
				return "fr";
				break;
			case "ITA":
			default:
				return "it";
				break;
		}

	}

	// load confiugration from the current environment
	private function loadConf() {
		$this->_dblink = new tc_db();
		$confloader = new tc_config();
		$this->_conf = $confloader->getConf();
		$_messages = new messages();
		$_messages->load();
		$this->messages = $_messages->items;
		$this->currLang = $this->getCurrLang();
		$this->_dblink->connect($this->_conf["database"]);
	}

}
